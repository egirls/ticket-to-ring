import random
from datetime import datetime

DRAW_SECONDS = 15 * 60

goals = []

with open("goals.txt") as file:
    goals = [line.rstrip() for line in file]

hand = []
last_draw = datetime.fromtimestamp(0)


def draw(num):
    global last_draw
    difference = datetime.now() - last_draw
    seconds = difference.total_seconds()
    if seconds > DRAW_SECONDS:
        last_draw = datetime.now()
        for i in range(0, num):
            goal = random.choice(goals)
            goals.remove(goal)
            hand.append(goal)
    else:
        print(f"It's only been {seconds} seconds!")


while True:
    prompt = input("> ")
    with open("hand.txt", "r+") as file:
        hand = [line.rstrip() for line in file]
    if prompt == "exit":
        break
    if prompt == "draw":
        if len(hand) == 0:
            draw(5)
        else:
            draw(3)
    display_hand = ('\n').join(hand)
    print(display_hand)
    with open("hand.txt", "w") as f:
        f.write(display_hand)
