1 Complete a Catacomb
1 Complete a Cave
1 Complete an Evergaol
1 Complete a Tunnel
1 Defeat a Limgrave boss
1 Defeat an invading NPC
1 Feed Gurranq Deathroot
1 Kill an Erdtree Avatar
1 Kill a Tibia Mariner
1 Kill Ensha
1 Kill Patches
1 Kill White Mask Varré
1 Return Boc’s sewing needle
1 Spare Patches
2 Acquire a legendary armament
2 Acquire a legendary incantation
2 Acquire an Heirloom Talisman
2 Acquire Margit’s Shackle
2 Defeat a boss while on Torrent for the whole fight
2 Defeat a Caelid boss
2 Defeat a Liurnia boss
2 Defeat a Weeping Penninsula boss
2 Duplicate a Remembrance at a Mausoleum
2 Feed Hyetta
2 Find a Whetblade (not the Whetstone Knife)
2 Kill an Erdtree Watchdog
2 Kill a Night’s Cavalry
2 Kill an Omenkiller boss
2 Kill D, Hunter of the Dead – or let Fia do it
2 Kill the Grafted Scion (Chapel of Anticipation)
2 Kill the Limgrave Tree Sentinel with a +0 weapon
2 Restore a Great Rune
2 Return the Chrysalid’s Memento to Roderika
2 Summon Blaidd to kill Darriwil
3 Acquire a legendary sorcery
3 Acquire a Scorpion Charm
3 Defeat an Altus Plateau boss
3 Defeat Blueretta (Spirit Loretta)
3 Earn Latenna’s trust
3 Find both halves of the Dectus Medallion
3 Have the Jar Bairn invite you to pick flowers
3 Hold hands with Rya 🥰 (be transported to Volcano Manor)
3 Kill a Deathbird or Death Rite Bird
3 Kill a Putrid or Ulcerated Tree Spirit
3 Kill Commander O’Neil
3 Kill Godrick while summoning Nepheli Loux
3 Kill the Leonine Misbegotten
3 Kill Wormface
3 Nyaaah (kill a dog-shaped boss with a claw weapon)
3 Use Dragon Hearts to purchase an incantation
4 Acquire both Scarseal talismans
4 Complete a Hero's Grave
4 Defeat a Remembrance boss claws, daggers, or fists only
4 Defeat a Remembrance boss colossal armaments only
4 Dudes Rock (buy crab from Big Boggart)
4 Enter Altus Plateau for the first time via Ruin-Strewn Precipice
4 Flat Fuck Friday (kill a Magma Wyrm with a hammer)
4 Give Thops an academy key
4 Kill a Dragonkin Soldier
4 Kill Elemer of the Briar
4 Kill Gurranq ;_;7
4 Upgrade a regular weapon to +12
5 Acquire the Black Knife
5 Acquire the pizza cutter (Ghiza’s Wheel)
5 Defeat Rennala after she summons all 4 different spirits
5 Defeat Rennala or Malenia with the Sword of St Trina
5 Finish off a boss with poison damage applied by Land Squirt Ashes
5 Get Alexander unstuck twice
5 Kill a Black Knife Assassin with the Cinquedea
5 Kill Radahn
5 Upgrade a somber weapon to +9
6 Acquire Mohg's Shackle
6 Defeat a Remembrance boss incantations only
6 Defeat a Remembrance boss sorcery only
6 Defeat Goldfrey (Spirit Godfrey)
6 Give Millicent a new arm
6 Have Boc alter Alberich’s Pointed Hat
6 Infuse the Prelate's Inferno Crozier with Bleed and kill a boss (Power Chainsawman RP)
6 Kill a Mad Pumpkin Head while wearing a Jar or Pumpkin Helm on your own head
6 Kill a Mimic Tear
6 Kill the Dung Eater
7 Defeat Baby Mohg (Sewer Mohg (Mohg, the Omen))
7 Kill Rykard
7 Receive Sellen's Primal Glintstone
8 Acquire the Blade of Calling
8 Confront Ranni about the Night of Black Knives
8 Give Ranni the Fingerslayer Blade
8 Tell Boc he's beautiful
9 Kill a God or Demigod with the Godslayer’s Greatsword
9 Kill Astel while wearing the Snow Witch Set
9 Kill Morgott
9 Put Nepheli Loux on the throne
9 Set Hyetta on fire ;_;7
